# Haydens README

Hi I'm Hayden, I'm currently a [Strategic Customer Success Manager](https://about.gitlab.com/job-families/sales/customer-success-management/#customer-success-manager-csm-intermediate). This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

# Related Pages

https://www.linkedin.com/in/hayden-matthews-86a3a64/ 

# About Me

I joined GitLab in November 2022 as a Customer Success Manager and will be focusing on NEUR customers. Prior to GitLab I worked at Invicti who offer DAST and IAST solutions. The software integrated well with GitLab, so I was aware of the company before joining. My background has mainly been enterprise sales for software security companies. 

I am married with 2 daughters, and the eldest has just become a teenager, so that should be a fun journey! I enjoy being active and getting outside, swimming, both in pools and open water. I love watching sport, football and rugby, but am happy watching almost any sport.

# How I Work

I mainly work 8.30am to 5.30pm Monday to Friday. The best way to connect with me is via Slack or email, but phone is also an option, I love to speak with people.
